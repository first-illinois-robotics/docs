
### `ansible/`

This contains a few Ansible playbooks that are called during the gitlab CI
pipeline. The three other playbooks are called from `bootstrap.yml` on
the deployment server, with templates from the `ansible/templates/` folder
being used to create files on the deployed server.

### `terraform/`

This contains the Hashicorp Ansible markup that defines the per-deployment
server environment. This references some "static" resources that are not deployed
per-environment (such as a subnet). This gets built and deployed automatically
as a part of the Gitlab CI pipeline.

### `firweb/`

This is the "entrypoint" of the site. This is a Django project that contains the setup
for Django CMS, as well as many utility functions.

#### `firweb/fixtures`

This is the location of the bootstrap data for a development environment. See the 
development environment documentation for this.

#### `firweb/settings`

The Django settings is represented by a Module instead of a file for easy 
environment-specific settings. The `__init__.py` gets loaded by Django,
which in turn will load `common.py` for shared settings, and then load the overrides
in `dev.py` or `prod.py` (which is used for all deployed environments, such as staging,
counterintuitively)

#### `firweb/static`

This includes the static content (in this case, images) that included in the bare templates
for the firweb site (i.e. the templates in `firweb/templates/`). Everything that's not an 
image is rendered as a part of the yarn build pipeline (in `frontend/`), including CSS,
Javascript, and some images (such as the multicolor program hamburger menu). Those static
images are included as well (in `frontend/dist/` which is only generated after `yarn build`).

## `events/`

This is another Django app, which `firweb` depends on. This is focused on cataloging team and
event history. There is a very well-defined database schema in `events/models.py`

### `events/scrapers`

There are several cron jobs that run on the server at various times. These cron jobs are
defined in `ansible/cron.yml`. Each of these run a Django management command in 
`events/management/commands`. These correspondingly call a scraper in `events/scrapers`. 

### `frontend/`

This is a Typescript project that's set up to generate all the required HTML, CSS, and some
other assets for the site.

#### `frontend/sass`

All the stylesheets for this site are written in Sass/Scss. The `index.scss` is the entry point,
which includes some variables first, the Bootstrap 5 source sass, fontawesome icons,
and then other bespoke files. The `fir_mobile_open.svg` file is compiled to be inline on build,
as it's included in `nav-brand.scss`. This is inlined on production using the `inline` function
found in `webpack.prod.js`.

#### `frontend/ts`

This is the typescript source for the various javascript-based functions of the site, which
there are very little (almost everything is handeled exclusively by the backend). `index.ts` is
the entry point. 

#### `asset-tags.ejs`

This file gets called as a part of the Webpack build, and rendered to 
`firweb/templates/generated/asset-tags.html`. This essentially renders into a Django template, which
is included in the other templates. This gets rendered by `HtmlWebpackPlugin` to 
add all the header and body tags to django-sekizai. 

#### `webpack.*.js`

Webpack is out JS ecosystem build tool of choice. Much like the Django app settings, the webpack 
config is split up based on the environment and use case. All the webpack settings are inherited
from `webpack.common.js` and then overrides and additions from `webpack.dev.js`, `webpack.prod.js`,
and `webpack.watch.js` and included for `yarn run build_dev`, `yarn run build` and `yarn run watch`,
respectively. 

#### Assorted JS Ecosystem files

- `.browserslistrc` - defines the browsers that are supported by the automed build tool
- `.eslintignore` - files and folders for `eslint` to ignore
- `.eslintrc.js` - adding plugins and config to `eslint`
- `.stylelintrc.json` - configuration for SCSS (style) linting
- `postcss.json` - defines CSS render pipeline
- `tsconfig.json` - configuration for TS to JS build pipeline

### `Dockerfile`

This creates a docker image for the build system `as used in `.gitlab-ci.yml`. This is not
used in production at all. 